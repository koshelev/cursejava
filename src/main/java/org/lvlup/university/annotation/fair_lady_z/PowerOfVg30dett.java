package org.lvlup.university.annotation.fair_lady_z;

import org.lvlup.university.reflect.RandomInt;
import org.lvlup.university.reflect.RandomString;
import org.lvlup.university.reflect.ReflectionClass;

@ReflectionClass
public class PowerOfVg30dett {

    int hpVol;
    @RandomInt
    int maxTorque;
    int maxRpm;

    @RandomString(isLowerCase = false)
    String name;

    public PowerOfVg30dett() {
        hpVol = 330;
        maxTorque = 388;
        maxRpm = 7000;
        name = "FairLady z32";
    }

    @Override
    public String toString() {
        return "PowerOfVg30dett{" +
                "hpVol=" + hpVol +
                ", maxTorque=" + maxTorque +
                ", maxRpm=" + maxRpm +
                ", name=" + name +
                '}';
    }

}
