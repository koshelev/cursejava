package org.lvlup.university.annotation.skyline;

import org.lvlup.university.reflect.RandomInt;
import org.lvlup.university.reflect.RandomString;
import org.lvlup.university.reflect.ReflectionClass;

@ReflectionClass
public class PowerOfRb26dett implements Skyline {
    int hpVol;
    int maxTorque;
    @RandomInt(minArgs = 8000, maxArgs = 10000)
    int maxRpm;

    @RandomString(maxStringLen = 10, isLowerCase = true)
    String name;

    public PowerOfRb26dett() {
        hpVol = 320;
        maxTorque = 392;
        maxRpm = 9000;
        name = "Skyline r34 gt";
    }

    @Override
    public String toString() {
        return "PowerOfRb26dett{" +
                "hpVol=" + hpVol +
                ", maxTorque=" + maxTorque +
                ", maxRpm=" + maxRpm +
                ", name=" + name +
                '}';
    }
}
