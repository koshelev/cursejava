package org.lvlup.university.annotation.skyline;

public interface Skyline {
    String toString();
}
