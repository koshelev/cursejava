package org.lvlup.university.annotation.civic_eg;

import org.lvlup.university.reflect.RandomInt;

public class PowerOfVtec {

    @RandomInt
    int hpVol;
    int maxTorque;
    int maxRpm;

    //@RandomInt
    String name;

    public PowerOfVtec() {
        hpVol = 200;
        maxTorque = 176;
        maxRpm = 8200;
        name = "Civic eg6";
    }

    @Override
    public String toString() {
        return "PowerOfVtec{" +
                "hpVol=" + hpVol +
                ", maxTorque=" + maxTorque +
                ", maxRpm=" + maxRpm +
                ", name=" + name +
                '}';
    }

}
