package org.lvlup.university.annotation;
import org.apache.commons.lang.RandomStringUtils;
import org.lvlup.university.reflect.RandomInt;
import org.lvlup.university.reflect.RandomString;
import org.lvlup.university.reflect.ReflectionClass;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Locale;
import java.util.stream.Stream;

public class annotation {

    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        processAnnotation("org.lvlup.university.annotation");

        System.out.println("Random Int annotation");
        randomIntProcessAnnotation("org.lvlup.university.annotation");

        System.out.println("Random String annotation");
        randomStringProcessAnnotation("org.lvlup.university.annotation");
    }

     public static void processAnnotation(String packageName) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
         ArrayList<String> files = searchFiles(packageName);

         for (String file : files) {
             Class<?> fileClass = Class.forName(file);
             if(!fileClass.isInterface() && fileClass.isAnnotationPresent(ReflectionClass.class)) {
                 Object obj = fileClass.getDeclaredConstructor().newInstance();
                 System.out.println(obj.toString());
             }
         }

    }

    public static void randomIntProcessAnnotation(String packageName) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        ArrayList<String> files = searchFiles(packageName);

        for (String file : files) {
            Class<?> fileClass = Class.forName(file);
            if(!fileClass.isInterface()) {
                Field[] fields = fileClass.getDeclaredFields();
                for(Field field: fields){
                    if(field.isAnnotationPresent(RandomInt.class)){
                        if(field.getType().equals(Integer.TYPE)){
                            RandomInt randomInt = field.getAnnotation(RandomInt.class);
                            int minVal = randomInt.minArgs();
                            int maxVal = randomInt.maxArgs();
                            int randomVal = (int) (Math.random() * ++maxVal) + minVal;
                            field.setAccessible(true);
                            Object obj = fileClass.getDeclaredConstructor().newInstance();
                            field.set(obj, randomVal);
                            System.out.println(obj.toString());
                        } else {
                            throw new RuntimeException("The field with name - " + field.getName() + " is not Integer/int");
                        }
                    }
                }
            }
        }

    }

    public static void randomStringProcessAnnotation(String packageName) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        ArrayList<String> files = searchFiles(packageName);

        for (String file : files) {
            Class<?> fileClass = Class.forName(file);
            if(!fileClass.isInterface()) {
                Field[] fields = fileClass.getDeclaredFields();
                for(Field field: fields){
                    if(field.isAnnotationPresent(RandomString.class)){
                        if(field.getType() == String.class){
                            RandomString randomString = field.getAnnotation(RandomString.class);
                            int maxStringLen = randomString.maxStringLen();
                            boolean isLowerCase = randomString.isLowerCase();

                            String rndString = RandomStringUtils.randomAlphabetic(maxStringLen);

                            if(isLowerCase){
                                rndString = rndString.toLowerCase(Locale.ROOT);
                            } else{
                                rndString = rndString.toUpperCase(Locale.ROOT);
                            }

                            field.setAccessible(true);
                            Object obj = fileClass.getDeclaredConstructor().newInstance();
                            field.set(obj, rndString);
                            System.out.println(obj.toString());
                        } else {
                            throw new RuntimeException("The field with name - " + field.getName() + " is not String");
                        }
                    }
                }
            }
        }

    }

    public static ArrayList<String> searchFiles(String packageName) throws IOException {
        File directory = new File("src/main/java/");
        try (Stream<Path> paths = Files
                .walk(Paths.get(directory.getAbsolutePath() +"//" + packageName.replace(".","\\")))
                .filter(p -> p.getFileName().toString().endsWith(".java"))
        ) {
            ArrayList<String> files = new ArrayList<>();
            for (Path file : (Iterable<Path>) paths::iterator) {
                files.add(file.toString()
                        .replace(".java","")
                        .replace(directory.getAbsolutePath(),"")
                        .replace("\\",".")
                        .replaceFirst(".","")
                );
            }
            return files;
        }
    }
}
