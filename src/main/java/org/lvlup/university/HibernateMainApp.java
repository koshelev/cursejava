package org.lvlup.university;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.lvlup.university.Repository.hbm.HibernateFacultyRepository;
import org.lvlup.university.Repository.hbm.HibernateSubjectRepository;
import org.lvlup.university.Repository.hbm.HibernateUniversityRepository;
import org.lvlup.university.configuration.DatabaseConfiguration;
import org.lvlup.university.configuration.HibernateConfiguration;
import org.lvlup.university.domain.Subjects;
import org.lvlup.university.domain.hbmFaculty;
import org.lvlup.university.domain.University;
import org.lvlup.university.reflect.AnnotationConfigurationPropertiesProcessor;

import java.util.List;

public class HibernateMainApp {

    public static void main(String[] args) {
        String confFileName = "database.properties";
        AnnotationConfigurationPropertiesProcessor.processorConfigurationFile(confFileName);
        System.out.println("Load all configuration files");

        HibernateConfiguration.configure(DatabaseConfiguration.getInstance());
        System.out.println("Hibernate conf sucs\n");
        SessionFactory factory = HibernateConfiguration.getSessionFactory();
        System.out.println("University app start\n");


        HibernateUniversityRepository universityRepository = new HibernateUniversityRepository(factory);
        universityRepository.deleteUniversity(2130);

        University updtUniv = new University((long)2030, "Сальный универ", "СУВ2", 1229 );
        universityRepository.updateUniversity((long)2030, updtUniv);
        universityRepository.updateUniversity((long)2030, (long)2030, "Сальный универ", "СУВ228", 1229);
        University foundU = universityRepository.findByuniversityid(1228);
        System.out.println(foundU);

        List<University> allUniver = universityRepository.findAllUniversity();
        for(University university: allUniver){
            System.out.println("Faculty cnt - " + university.getFaculties().size() + " for univercityId - " +university.getUniversityid());
        }

        HibernateFacultyRepository facultyRepository = new HibernateFacultyRepository(factory);

        List<hbmFaculty> facultyList = facultyRepository.findHbmFaculty(2160);
        for(hbmFaculty faculty: facultyList){
            System.out.println(faculty);
        }

        HibernateSubjectRepository subjectRepository = new HibernateSubjectRepository(factory);
        Subjects subject = subjectRepository.createSubject("Fat maker11212", 18);
        System.out.println(subject);

        Subjects subjectF = subjectRepository.createSubjectWithFaculty("Fat makerssss1s112288", 22, 5 );
        System.out.println(subjectF);

        List<hbmFaculty> facultySubjects = facultyRepository.findAllSubjectsByFaculty(2);
        for(hbmFaculty facultySubject : facultySubjects){
           System.out.println("Subjects for facultyId - "
                   + facultySubject.getId()
                   + " :" + facultySubject.getSubjects());
        }

        int facultyId = 6;
        hbmFaculty faculty = null;
        try(Session s = factory.openSession()){
            faculty = s.get(hbmFaculty.class, facultyId);
        } catch(Exception exc){
                throw new RuntimeException("FacultyId -" + facultyId + " is not exists, exception - " + exc);
        }

        int subjectId = 25;
        Subjects subjects = null;
        try(Session s = factory.openSession()){
            subjects = s.get(Subjects.class, subjectId);
        } catch(Exception exc){
            throw new RuntimeException("SubjectsId -" + subjectId + " is not exists, exception - " + exc);
        }

        if(subjects == null || faculty == null) {
            throw new RuntimeException("SubjectsId or FacultyId is not exists");
        }
        subjectRepository.setSubjectWithFaculty(subjects, faculty.getId());


  /*      University getU = null;
        University loadU = null;

        try(Session s = factory.openSession()){
           getU = s.get(University.class, 1228L);
        }
        try(Session s = factory.openSession()){
            loadU = s.load(University.class, 1228L);
            System.out.println(loadU);
        }

        System.out.println(getU);
*/

//        HibernateFacultyRepository facultyRepository = new HibernateFacultyRepository(factory);
//        hbmFaculty faculty = facultyRepository.createFaculty("Salo2282", 1228);
//        System.out.println(faculty);


//        HibernateUniversityRepository universityRepository = new HibernateUniversityRepository(factory);
//        University university = universityRepository.createUniversity("Сальный цивикостроительный vtec k24"
//        ,"СЦСVTECk24"
//        ,1228
//        ,List.of("Сало228", "Цивик228"));

  //      System.out.println(university);

        factory.close();
    }

}
