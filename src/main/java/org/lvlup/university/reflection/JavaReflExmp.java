package org.lvlup.university.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;

public class JavaReflExmp {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Subject subject = new Subject("Программирование");
        Object obj = subject;
        Class<?> obClass = obj.getClass();

        Class<?> subjectClass = subject.getClass();
        Class<?> subClass =Subject.class;

        Field[] fields = subClass.getDeclaredFields();
        for(Field field:fields){
            System.out.println(field.getType().getName() +" " + field.getName());
        }

        System.out.println("Constr");
        Constructor<?>[] constructors = subClass.getDeclaredConstructors();
        for(Constructor<?> constructor: constructors){
          Class<?>[] types =  constructor.getParameterTypes();
            System.out.println(Arrays.toString(types));
        }
        Field subjectNamefield = subjectClass.getDeclaredField("name");
        subjectNamefield.setAccessible(true);
        String subjectName = (String) subjectNamefield.get(subject);
        System.out.println(subjectName);
        subjectNamefield.set(subject, "salo");
        System.out.println((String) subjectNamefield.get(subject));
    }
}
