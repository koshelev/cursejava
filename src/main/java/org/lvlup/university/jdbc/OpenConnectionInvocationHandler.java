package org.lvlup.university.jdbc;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

@RequiredArgsConstructor
public class OpenConnectionInvocationHandler implements InvocationHandler {
    private final OriginalDatabaseService databaseService;

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        long startConnectionTime = System.nanoTime();
        String methodName = method.getName();
        if(methodName.equals("openConnection")) {
            Object result = method.invoke(databaseService, objects);
            long startTime = System.nanoTime();
            ConnectionTime.getInstance().put((Connection) result, startTime);
            System.out.println("Open connection time - " + (System.nanoTime() - startConnectionTime));
            return result;
        }
        return method.invoke(databaseService, objects);
    }
}
