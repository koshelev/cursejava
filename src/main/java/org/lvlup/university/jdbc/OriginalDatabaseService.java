package org.lvlup.university.jdbc;

import org.lvlup.university.Repository.DatabaseService;
import org.lvlup.university.configuration.DatabaseConfiguration;
import org.lvlup.university.reflect.ConnectionTime;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OriginalDatabaseService implements DatabaseService {

    private DatabaseConfiguration dbConfiguration;
    private ConnectionPool connectionPool;

    public OriginalDatabaseService(DatabaseConfiguration dbConfiguration){
        this.dbConfiguration = dbConfiguration;
        this.connectionPool = new ConnectionPool();
    }

    @Override
    public void fillPool(){
        for(int i = 0; i < dbConfiguration.getMinPoolSize(); i++){
            Connection connection = createConnection();
            connectionPool.returnConnection(connection);
        }
    }

    private Connection createConnection(){
        try{
            return DriverManager.getConnection(
                    dbConfiguration.getUrl(),
                    dbConfiguration.getLogin(),
                    dbConfiguration.getPassword()
            );
        }catch (SQLException exc){
            throw new RuntimeException(exc);
        }
    }

    @Override
    @ConnectionTime
    public Connection openConnection() {
        Connection connection =  connectionPool.getConnection();
        return (Connection) Proxy.newProxyInstance(
            connection.getClass().getClassLoader(),
            connection.getClass().getInterfaces(),
            new ConnectionInvocationHandler(connection, connectionPool)
        );
    }
}
