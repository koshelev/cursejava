package org.lvlup.university.jdbc;

public class CustomSqlException extends Exception {
    public CustomSqlException(String errorMessage){
        super(errorMessage);
    }
}
