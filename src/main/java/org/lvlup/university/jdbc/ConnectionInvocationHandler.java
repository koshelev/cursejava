package org.lvlup.university.jdbc;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

public class ConnectionInvocationHandler implements InvocationHandler {

    public  final Connection originalConnection;
    private final ConnectionPool connectionPool;

    public ConnectionInvocationHandler(Connection originalConnection, ConnectionPool connectionPool) {
        this.originalConnection = originalConnection;
        this.connectionPool = connectionPool;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws  Throwable{
        String methodName = method.getName();
        if(methodName.equals("close")){
            connectionPool.returnConnection(originalConnection);
            long startTime = ConnectionTime.getInstance().get(originalConnection);
            System.out.println("Connection wasn't in poll = " + (System.nanoTime() - startTime));
            return null;
        }
       return method.invoke(originalConnection, args);
    }
}
