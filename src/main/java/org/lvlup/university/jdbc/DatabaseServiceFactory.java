package org.lvlup.university.jdbc;

import org.lvlup.university.Repository.DatabaseService;
import org.lvlup.university.configuration.DatabaseConfiguration;

import java.lang.reflect.Proxy;

public class DatabaseServiceFactory {
    public static DatabaseService getDatabaseService() {
        return (DatabaseService) Proxy.newProxyInstance(
                OriginalDatabaseService.class.getClassLoader(),
                OriginalDatabaseService.class.getInterfaces(),
                new OpenConnectionInvocationHandler(new OriginalDatabaseService(DatabaseConfiguration.getInstance()))
        );
    }

}
