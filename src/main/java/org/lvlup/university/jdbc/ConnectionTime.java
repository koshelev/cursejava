package org.lvlup.university.jdbc;

import java.sql.Connection;
import java.util.IdentityHashMap;
import java.util.Map;

public final class ConnectionTime {

    private ConnectionTime() {}
    private static final ConnectionTime INSTANCE = new ConnectionTime();

    public static ConnectionTime getInstance() {
        return INSTANCE;
    }

    private Map<Connection, Long> openConnection = new IdentityHashMap<>();

    public void put(Connection connection, long startTime) {
        openConnection.put(connection, startTime);
    }

    public long get(Connection connection) {
        Long[] startTime = new Long[1];
        openConnection.forEach((key, value )-> {
            if(key.equals(connection)) {
                startTime[0] = value;
            }
        });

        return startTime[0] == null ? 0 : startTime[0];
    }

}
