package org.lvlup.university.threads.interaption;

import lombok.SneakyThrows;

public class ThredInterraption {

    @SneakyThrows
    public static void main(String[] args) {
        Thread thread = new Thread(new Worker());
        thread.start();

        System.err.println("Send siganl int 1 ");
        thread.interrupt();
        Thread.sleep(1500);

        System.err.println("Send siganl int 2");
        thread.interrupt();
        Thread.sleep(1500);

        System.err.println("Send siganl int 3");
        thread.interrupt();
        Thread.sleep(1500);
    }
}

