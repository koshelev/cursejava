package org.lvlup.university.threads.interaption;

public class Worker implements Runnable{

    @Override
    public void run(){
        int interruptCallCount = 0;
        boolean isInterrupted = false;

        while(true){
            try {

                if(isInterrupted){
                    interruptCallCount ++;
                    System.out.println("Cont of interrupted" + interruptCallCount);
                    if(interruptCallCount >= 3){
                        System.out.println("Thread finished works");
                        return;
                    }
                }
                isInterrupted = Thread.interrupted();

                System.out.println("Thread working");
                Thread.sleep(700);
            } catch(InterruptedException exc){
                System.out.println("Thread have been interrupted");
                isInterrupted = true;
            }
        }
    }
}
