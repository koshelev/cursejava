package org.lvlup.university.threads.pc;

import lombok.SneakyThrows;

public class PcApp {

@SneakyThrows
    public static  void main(String[] args){
        StatisticRequestQueue queue = new RientrantLockStatisticsRequstQueue(5);
        Thread consumer1 = new Thread(new StatisticRequestConsumer(queue), "consumer1");
        Thread consumer2 = new Thread(new StatisticRequestConsumer(queue), "consumer2");

        consumer1.start();
        consumer2.start();

        Thread producer1 = new Thread(new StatisticRequestProducer(queue), "producer1");
        Thread producer2 = new Thread(new StatisticRequestProducer(queue), "producer2");

        producer1.start();
        producer2.start();

        Thread.sleep(30000);
        System.err.println("Send interr singal to cons");
        consumer1.interrupt();
        consumer2.interrupt();
    }
}
