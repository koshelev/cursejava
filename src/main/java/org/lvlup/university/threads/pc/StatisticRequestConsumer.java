package org.lvlup.university.threads.pc;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class StatisticRequestConsumer implements Runnable{
    private final StatisticRequestQueue queue;

    @Override
    @SneakyThrows
    public void run(){
        boolean isInterrupted = false;

        while (!isInterrupted ||  !queue.isEmpty()){
            try {
                String accauntNumber = queue.take();
                System.err.println("Consumer " + Thread.currentThread().getName() + "start collect statist for the accont " + accauntNumber);
                Thread.sleep(1500);
                System.err.println("Consumer " + Thread.currentThread().getName() + "collected statistics for accaunt - " + accauntNumber);
              if(!isInterrupted) {
                  isInterrupted = Thread.interrupted();
              }
            } catch(InterruptedException exc){
                System.out.println("Thread has been interrupted");
                isInterrupted = true;
            }

        }
    }
}
