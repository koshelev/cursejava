package org.lvlup.university.threads.pc;

public interface StatisticRequestQueue {

    void offer(String accountNumber) throws InterruptedException;
    String take() throws InterruptedException;
    boolean isEmpty();
}
