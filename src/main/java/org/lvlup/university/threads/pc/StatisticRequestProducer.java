package org.lvlup.university.threads.pc;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.util.Random;

@RequiredArgsConstructor
public class StatisticRequestProducer implements Runnable{
    private final StatisticRequestQueue queue;

    @Override
    @SneakyThrows
    public void run(){
        Random r = new Random();
        for(int i = 0; i < 10; i++){
            int accountNumber = r.nextInt(10000000) + 1000000;
            System.out.println("Producer" + Thread.currentThread().getName() + "put with acc" + accountNumber);
            queue.offer(String.valueOf(accountNumber));
            Thread.sleep(1000);
        }

    }
}
