package org.lvlup.university.threads;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

public class CounterExmpl {

    @SneakyThrows
    public static void main(String[] args) {
        //Counter synchronizeCounter = new SynchronizeCounter();
        //Counter synchronizeCounter = new ReentrantLoockCounter();
        Counter synchronizeCounter = new NonBlockingCounter();
        Thread tr1 = new Thread(new Incrementor(synchronizeCounter));
        Thread tr2 = new Thread(new Incrementor(synchronizeCounter));
        Thread tr3 = new Thread(new Incrementor(synchronizeCounter));

        tr1.start();
        tr2.start();
        tr3.start();

        tr1.join();
        tr2.join();
        tr3.join();

        System.out.println("Counter val =  " + synchronizeCounter.getValue());
    }


    static class SynchronizeCounter implements Counter{

        private final Object mutex = new Object();

        private int value;
        public void increment(){
            synchronized (mutex) {
                value++;
            }
        }

        public synchronized void syncrIncr(){
            value++;
        }

        public int getValue(){
            return value;
        }
    }

    @RequiredArgsConstructor
    static class Incrementor implements Runnable{
        private final Counter synchronizeCounter;

        @Override
        @SneakyThrows
        public void run() {
            for(int i = 0; i < 40; i++){
                synchronizeCounter.increment();
                Thread.sleep(100);
            }
        }
    }


}
