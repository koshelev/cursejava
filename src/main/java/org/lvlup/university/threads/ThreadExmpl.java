package org.lvlup.university.threads;

import lombok.SneakyThrows;

import java.time.LocalTime;

public class ThreadExmpl {

    @SneakyThrows
    public static void main(String[] args) {
        Thread timePrinter = new Thread(new TimePrinter());
        timePrinter.setDaemon(true);
        timePrinter.start();

        String mainThreadName = Thread.currentThread().getName();
        System.out.println(mainThreadName + " - Main salo");
        Thread thread = new Printer();
        thread.start();

        Thread threadF = new Thread(new FactorialCalc(), "Fact thread");
        threadF.start();

        Thread threadF2 = new Thread(new FactorialCalc(), "Fact thread2");
        threadF2.start();

        Thread threadF3 = new Thread(new FactorialCalc(), "Fact thread3");
        threadF3.start();

        thread.join();
        Thread.sleep(2500);
        System.out.println(mainThreadName + "- End of method main");
    }

    static class Printer extends Thread{

        public Printer(){
            super("Printer thread");
        }

        @SneakyThrows
        @Override
        public void run(){

            String threadName = getName();
            for(int i = 0; i < 10; i++){
                System.out.println(threadName + "- salo");
                Thread.sleep(400);
            }
            System.out.println(threadName + "- I ate all fat");
        }
    }

    static class FactorialCalc implements Runnable{

        @Override
        @SneakyThrows
        public void run(){
            int fact = 1;

            for(int i = 1; i <9; i++){
                fact *= i;
            }
            Thread.sleep(600);
            System.out.println(Thread.currentThread().getName() + "- fact = " + fact);
        }
    }

    static class TimePrinter implements Runnable {
        @Override
        @SneakyThrows
        public void run(){
            while (true){
                System.out.println(LocalTime.now());
                Thread.sleep(1000);
            }
        }
    }

}
