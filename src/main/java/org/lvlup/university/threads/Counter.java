package org.lvlup.university.threads;

public interface Counter {

    void increment();
    int getValue();
}
