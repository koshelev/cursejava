package org.lvlup.university.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Data
@Entity
@Table(name ="tsubject")
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "faculties")
public class Subjects {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private int hours;

    public Subjects(String name, int hours){
        this.name= name;
        this.hours = hours;
        this.facultySubjects = new ArrayList<>();
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    @JoinColumn(name = "subjectid", updatable = false)
    private List<FacultySubject> facultySubjects;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "tfacultysubject"
    ,joinColumns = @JoinColumn(name ="subjectid")
    ,inverseJoinColumns = @JoinColumn(name = "facultyid")
    )
    private List<hbmFaculty> faculties;

}
