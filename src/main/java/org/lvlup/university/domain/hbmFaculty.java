package org.lvlup.university.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "tfaculties")
@ToString(exclude = {
        "university"
        ,"subjects"
})
public class hbmFaculty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;

    @ManyToOne
    @JoinColumn(name = "universityid")
    private University university;

    @ManyToMany(mappedBy = "faculties", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Subjects> subjects;
//
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "id")
//    private FacultyInfo facultyInfo;

}
