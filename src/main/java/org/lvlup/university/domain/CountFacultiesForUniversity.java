package org.lvlup.university.domain;

public class CountFacultiesForUniversity {
    long universityid;
    int facultiesCnt;

    public CountFacultiesForUniversity(long universityid, int facultiesCnt) {
        this.universityid = universityid;
        this.facultiesCnt = facultiesCnt;
    }

    public long getuniversityid() {
        return universityid;
    }

    public int getFacultiesCnt() {
        return facultiesCnt;
    }
}
