package org.lvlup.university.domain;

public class Faculty {
    int id;
    String name;
    long universityId;

    public Faculty(int id, String name, long universityId) {
        this.id = id;
        this.name = name;
        this.universityId = universityId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getUniversityid() {
        return universityId;
    }
}
