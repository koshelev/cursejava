package org.lvlup.university.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name ="tfacultyinfo")
@NoArgsConstructor
public class FacultyInfo {

    @Id
    private Integer faculty_id;
    private String phone;
    private String mail;

}
