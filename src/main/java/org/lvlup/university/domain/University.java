package org.lvlup.university.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "tuniversity")
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class University {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long universityid;

    @Column(name = "name")
    private String name;
    @Column(name = "short_name")
    private String shortName;
    @Column(name = "foundation_year")
    private Integer foundationYear;

    @OneToMany(mappedBy = "university", cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    private List<hbmFaculty> faculties;

    public University(String name, String shortName, Integer foundationYear) {
        this.name = name;
        this.shortName = shortName;
        this.foundationYear = foundationYear;
        this.faculties = new ArrayList<>();
    }

    public University(Long universityid, String name, String shortName, Integer foundationYear) {
        this.universityid = universityid;
        this.name = name;
        this.shortName = shortName;
        this.foundationYear = foundationYear;
        this.faculties = new ArrayList<>();
    }

}