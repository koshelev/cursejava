package org.lvlup.university.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "tfacultysubject")
@ToString(exclude = "subject")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class FacultySubject {
    @Id
    private Integer facultyid;
   // private Integer subjectid;


    @ManyToOne
    @JoinColumn(name = "subjectid")
    private Subjects subject;

//    @ManyToOne
//    @JoinColumn(name = "facultyid", insertable = false, updatable = false)
//    private hbmFaculty faculty;

}
