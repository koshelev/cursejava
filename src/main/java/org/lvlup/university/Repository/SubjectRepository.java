package org.lvlup.university.Repository;

import org.lvlup.university.domain.FacultySubject;
import org.lvlup.university.domain.Subjects;
import org.lvlup.university.domain.University;
import org.lvlup.university.domain.hbmFaculty;

import java.util.List;

public interface SubjectRepository {
    Subjects createSubject(String name, Integer hours);
    Subjects createSubjectWithFaculty(String name, Integer hours, Integer facultyId);
    List<FacultySubject> findAllSubjectsByFaculty(Integer facId);
    FacultySubject setSubjectWithFaculty(Subjects sbj, Integer facultyId);
}
