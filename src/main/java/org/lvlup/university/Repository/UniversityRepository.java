package org.lvlup.university.Repository;

import org.lvlup.university.domain.CountFacultiesForUniversity;
import org.lvlup.university.domain.University;
import org.lvlup.university.jdbc.CustomSqlException;
import java.util.List;

public interface UniversityRepository {
    List<University> findAllUniversity();

    University findByuniversityid(long universityid);

    University createUniversity(String name, String shortName, Integer foundationYear);
    University deleteUniversity(long id);
    University updateUniversity(Long id, University university) throws CustomSqlException;
    University updateUniversity(Long id, Long newId, String newName, String newShortName, Integer newFoundationYear) throws CustomSqlException;
    List<University> findUniversity(long id);
    List<CountFacultiesForUniversity> countFacultiesForUniversities();

    University createUniversity(String name, String shortName, Integer foundationYear, List<String> facultiesNames);
}
