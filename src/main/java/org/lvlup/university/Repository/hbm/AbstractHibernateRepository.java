package org.lvlup.university.Repository.hbm;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.function.Function;

@RequiredArgsConstructor
public abstract class AbstractHibernateRepository {

    protected final SessionFactory factory;

    protected <R> R runWithTransaction(Function<Session, R> invokeWithTransaction){
        try(Session session = factory.openSession()){
            Transaction tx =session.beginTransaction();
            R result = invokeWithTransaction.apply(session);
            tx.commit();
            return result;
        }
    }

}
