package org.lvlup.university.Repository.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.lvlup.university.Repository.FacultyRepository;
import org.lvlup.university.domain.Faculty;
import org.lvlup.university.domain.FacultySubject;
import org.lvlup.university.domain.hbmFaculty;
import org.lvlup.university.domain.University;
import java.util.List;

public class HibernateFacultyRepository extends AbstractHibernateRepository implements FacultyRepository  {

    public HibernateFacultyRepository(SessionFactory factory) {
        super(factory);
    }

    public hbmFaculty createHbmFaculty(String name, long universityid) {

           return runWithTransaction(session -> {
                hbmFaculty faculty = new hbmFaculty();
                faculty.setName(name);
                faculty.setUniversity(session.load(University.class, universityid));
                session.persist(faculty);
                return faculty;
            });
        }

    @Override
    public List<Faculty> findFaculty(long id) {
        return null;
    }

    @Override
    public List<hbmFaculty> findHbmFaculty(long universityid){
        return runWithTransaction(session -> {
            return session.createQuery("from hbmFaculty where universityid = :uid", hbmFaculty.class)
                    .setParameter("uid", universityid)
                    .getResultList();
        });
    }

    @Override
    public List<hbmFaculty> findAllSubjectsByFaculty(Integer facId){
        return runWithTransaction(session -> {
            return session.createQuery("from hbmFaculty where id = :uid", hbmFaculty.class)
                    .setParameter("uid", facId)
                    .getResultList(); //список строк преобр в объекты сущьности
        });
    }

    @Override
    public Faculty createFaculty(String name, long universityid) {
        return null;
    }

}

