package org.lvlup.university.Repository.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.lvlup.university.Repository.SubjectRepository;
import org.lvlup.university.domain.*;

import java.util.List;

public class HibernateSubjectRepository extends AbstractHibernateRepository implements SubjectRepository {

    public HibernateSubjectRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public Subjects createSubject(String name, Integer hours){
        return runWithTransaction(session -> {
            Subjects s = new Subjects(name, hours);
            session.persist(s); //insert
            System.out.println("New subject created with id - " + s.getId());
            return s;
        });
    }

    @Override
    public Subjects createSubjectWithFaculty(String name, Integer hours, Integer facultyId) {
        return runWithTransaction(session -> {
            Subjects s = new Subjects(name, hours);

            FacultySubject newFacultySubject = new FacultySubject();
            newFacultySubject.setFacultyid(facultyId);
            newFacultySubject.setSubject(s);
            s.getFacultySubjects().add(newFacultySubject);

            session.persist(s);

            return s;
        });
    }

    @Override
    public FacultySubject setSubjectWithFaculty(Subjects sbj, Integer facultyId) {
        return runWithTransaction(session -> {
            FacultySubject newFacultySubject = new FacultySubject();
            newFacultySubject.setFacultyid(facultyId);
            newFacultySubject.setSubject(sbj);
            session.persist(newFacultySubject);

            return newFacultySubject;
        });
    }

    @Override
    public List<FacultySubject> findAllSubjectsByFaculty(Integer facId){
        return runWithTransaction(session -> {
            return session.createQuery("from FacultySubject where facultyid = :uid", FacultySubject.class)
                    .setParameter("uid", facId)
                    .getResultList(); //список строк преобр в объекты сущьности
        });
    }
}
