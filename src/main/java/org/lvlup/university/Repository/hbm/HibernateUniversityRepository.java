package org.lvlup.university.Repository.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.lvlup.university.Repository.UniversityRepository;
import org.lvlup.university.domain.CountFacultiesForUniversity;
import org.lvlup.university.domain.University;
import org.lvlup.university.domain.hbmFaculty;
import org.lvlup.university.jdbc.CustomSqlException;

import javax.persistence.Query;
import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class HibernateUniversityRepository extends AbstractHibernateRepository implements UniversityRepository {

    public HibernateUniversityRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public List<University> findAllUniversity(){
        try(Session session = factory.openSession()) {
            return session.createQuery("from University", University.class)
                    .getResultList(); //список строк преобр в объекты сущьности
        }
    }

    @Override
    public University findByuniversityid(long universityid) {
        try(Session session = factory.openSession()) {
            return session.createQuery("from University where universityid = :uid", University.class)
                    .setParameter("uid", universityid)
                    .getSingleResult();
        }
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear){
        return runWithTransaction(session -> {
            University u = new University(name, shortName, foundationYear);
            session.persist(u); //insert
            System.out.println("ne univ id - " + u.getUniversityid());
            return u;
        });
    }

    @Override
    public University deleteUniversity(long id) {
        runWithTransaction(session -> {
            String sqlQuery = "delete from University where universityid = " + id;
            Query query = session.createQuery(sqlQuery);

            int cnt = query.executeUpdate();

            if(cnt > 0){
                System.out.println("Successfully deleted university with id - " + id);
            } else{
                System.out.println("This university is is not exist");
            }
            return null;
        });
        return null;
    }

    @Override
    public University updateUniversity(Long id, University university) {
        runWithTransaction(session -> {
            String queryString = "update University set universityid = :univerId, name = :name" +
                    ", shortName = :shortName, foundationYear = :foundYear"+
                    " where universityid = :filtrUniverId";

            int cnt = session.createQuery(queryString)
                    .setParameter("univerId", university.getUniversityid())
                    .setParameter("name", university.getName())
                    .setParameter("shortName", university.getShortName())
                    .setParameter("foundYear", university.getFoundationYear())
                    .setParameter("filtrUniverId", id)
                    .executeUpdate();

            if(cnt > 0){
                System.out.println("Successfully updated university with id - " + id + " cnt = " + cnt);
            } else{
                try {
                    throw new CustomSqlException("this university is not exists, update is impossible");
                } catch (CustomSqlException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });
        return null;
    }

    @Override
    public University updateUniversity(Long id, Long newId, String newName, String newShortName, Integer newFoundationYear) {
        runWithTransaction(session -> {
            String queryString = "update University set universityid = :univerId, name = :name" +
                    ", shortName = :shortName, foundationYear = :foundYear"+
                    " where universityid = :filtrUniverId";

            int cnt = session.createQuery(queryString)
                    .setParameter("univerId", newId)
                    .setParameter("name", newName)
                    .setParameter("shortName", newShortName)
                    .setParameter("foundYear", newFoundationYear)
                    .setParameter("filtrUniverId", id)
                    .executeUpdate();

            if(cnt > 0){
                System.out.println("Successfully updated university with id - " + id + " cnt = " + cnt);
            } else{
                try {
                    throw new CustomSqlException("this university is not exists, update is impossible");
                } catch (CustomSqlException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });
        return null;
    }

    @Override
    public List<University> findUniversity(long id) {
        return null;
    }

    @Override
    public List<CountFacultiesForUniversity> countFacultiesForUniversities() {
        return null;
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear, List<String> facultiesNames) {
       try(Session session = factory.openSession()){
        Transaction tx = session.beginTransaction();

        University university = new University(name, shortName, foundationYear);
        for(String facultyName :facultiesNames){
            hbmFaculty faculty = new hbmFaculty();
            faculty.setName(facultyName);
            faculty.setUniversity(university);

         university.getFaculties().add(faculty);

        }
        session.persist(university);

        tx.commit();

        return university;
     }
    }
}
