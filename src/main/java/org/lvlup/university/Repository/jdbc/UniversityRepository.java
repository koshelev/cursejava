package org.lvlup.university.Repository.jdbc;

import org.lvlup.university.Repository.DatabaseService;
import org.lvlup.university.domain.CountFacultiesForUniversity;
import org.lvlup.university.domain.University;
import org.lvlup.university.jdbc.CustomSqlException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import  java.util.List;

public class UniversityRepository implements org.lvlup.university.Repository.UniversityRepository {
    private final DatabaseService dbService;

    public UniversityRepository(DatabaseService dbService) {
        this.dbService = dbService;
    }
    @Override
    public  List<University> findAllUniversity(){
        try(Connection connection = dbService.openConnection()){
            Statement stm = connection.createStatement();
            ResultSet result = stm.executeQuery("select * from tuniversity");
            return (retrieveFromResultSet(result));

        } catch (SQLException exc){
             System.out.println("Could not get university -" + exc.getMessage());
             return Collections.emptyList();
        }
    }

    @Override
    public University findByuniversityid(long universityid) {
        return null;
    }

    @Override
    public  List<University> findUniversity(long id){
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM tuniversity WHERE id = ?");
            stm.setLong(1, id);
            ResultSet result = stm.executeQuery();
            return (retrieveFromResultSet(result));

        } catch (SQLException exc){
            System.out.println("Could not get university " + id +" - " + exc.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public List<CountFacultiesForUniversity> countFacultiesForUniversities() {
        try (Connection connection = dbService.openConnection()) {
            Statement stm = connection.createStatement();

            ResultSet result = stm.executeQuery("SELECT\n" +
                    "\t unv.id\n" +
                    "\t,COUNT(fac.id) AS facultiesCnt\n" +
                    "FROM public.tuniversity AS unv\n" +
                    "LEFT JOIN public.tfaculties AS fac\n" +
                    "\tON unv.id = \"universityid\"\n" +
                    "GROUP BY\n" +
                    "\tunv.id;");

            List<CountFacultiesForUniversity> countFaculties = new ArrayList<>();
            while (result.next()) {
                Long universityid = result.getLong("id");
                int facultiesCnt = result.getInt("facultiesCnt");

                CountFacultiesForUniversity countFacultiesForUniversity = new CountFacultiesForUniversity(universityid, facultiesCnt);
                countFaculties.add(countFacultiesForUniversity);
            }
            return (countFaculties);
        } catch(SQLException exc){
            System.out.println("Could not get faculties cnt for university -" + exc.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear, List<String> facultiesNames) {
        return null;
    }

    @Override
    public University createUniversity(String name, String shortName, Integer foundationYear) {
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("INSERT INTO public.tuniversity(name, short_name, foundation_year) VALUES (?, ?, ?);", Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, name);
            stm.setString(2, shortName);
            if(foundationYear == null){
                stm.setNull(3,JDBCType.INTEGER.getVendorTypeNumber());
            } else {
                stm.setInt(3, foundationYear);
            }

            int createdRows = stm.executeUpdate();
            System.out.println("Count of inserted rows - " + createdRows);

            ResultSet gentKeySet = stm.getGeneratedKeys();
            gentKeySet.next();
            Long universityid = gentKeySet.getLong(1);
            return new University(universityid, name, shortName, foundationYear);

        } catch (SQLException exc){
            System.out.println("Could not insert new university -" + exc.getMessage());
        }
        return null;
    }

    @Override
    public University deleteUniversity(long id) {
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("DELETE FROM public.tuniversity WHERE id = ?;");

            stm.setLong(1, id);
            int deletedRows = stm.executeUpdate();
            if(deletedRows > 0){
                System.out.println("Successfully deleted university with id - " + id);
            } else{
                System.out.println("This university is is not exist");
            }

        } catch (SQLException exc){
            System.out.println("Could not delete university -" + exc.getMessage());
        }
        return null;
    }

    @Override
    public University updateUniversity(Long id, University university) throws CustomSqlException {
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("UPDATE public.tuniversity SET id= ?, name= ?, short_name= ?, foundation_year= ? WHERE id= ?;");

            //new values
            stm.setLong(1, university.getUniversityid());
            stm.setString(2, university.getName());
            stm.setString(3, university.getShortName());
            if(university.getFoundationYear() == null){
                stm.setNull(4,JDBCType.INTEGER.getVendorTypeNumber());
            } else {
                stm.setInt(4, university.getFoundationYear());
            }

            //current values for where
            stm.setLong(5, id);

            updateUniversity(stm);

        } catch (SQLException exc) {
            System.out.println("Could not update -" + exc.getMessage());
        } catch (CustomSqlException exc) {
            throw exc;
        }
        return null;
    }

    @Override
    public University updateUniversity(Long id, Long newId, String newName, String newShortName, Integer newFoundationYear) throws CustomSqlException {
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("UPDATE public.tuniversity SET id= ?, name= ?, short_name= ?, foundation_year= ? WHERE id= ?;");

            //new values
            stm.setLong(1, newId);
            stm.setString(2, newName);
            stm.setString(3, newShortName);
            if(newFoundationYear == null){
                stm.setNull(4,JDBCType.INTEGER.getVendorTypeNumber());
            } else {
                stm.setInt(4, newFoundationYear);
            }

            //current values for where
            stm.setLong(5, id);

            updateUniversity(stm);

        } catch (SQLException exc) {
            System.out.println("Could not update -" + exc.getMessage());
        } catch (CustomSqlException exc) {
            throw exc;
        }
        return null;
    }

    public University updateUniversity(PreparedStatement stm) throws CustomSqlException {
        try{
            int updatedRows = stm.executeUpdate();

            if(updatedRows > 0) {
                System.out.println("Count of updated rows - " + updatedRows);
            } else {
                throw new CustomSqlException("this university is not exists, update is impossible");
            }
        } catch (SQLException exc){
             System.out.println("Could not update -" + exc.getMessage());
        } catch (CustomSqlException exc){
            throw exc;
        }
        return null;
    }

    private List<University> retrieveFromResultSet(ResultSet resultSet) throws  SQLException{
        List<University> universities = new ArrayList<>();
        while (resultSet.next()){
            Long universityid = resultSet.getLong("id");
            String name = resultSet.getString("name");
            String shortName = resultSet.getString("short_name");
            Integer foundationYear = resultSet.getInt("foundation_year");
            University university = new University(universityid ,name, shortName, foundationYear);
            universities.add(university);
        }
        return universities;
    }
}
