package org.lvlup.university.Repository.jdbc;

import org.lvlup.university.Repository.DatabaseService;
import org.lvlup.university.domain.Faculty;
import org.lvlup.university.domain.hbmFaculty;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FacultyRepository implements org.lvlup.university.Repository.FacultyRepository {

    private final DatabaseService dbService;
    public FacultyRepository(DatabaseService dbService) {
        this.dbService = dbService;
    }

    @Override
    public List<Faculty> findFaculty(long universityid){
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM tfaculties WHERE \"universityid\" = ?;");
            stm.setLong(1, universityid);
            ResultSet result = stm.executeQuery();
            return (retrieveFromResultSet(result));

        } catch (SQLException exc){
            System.out.println("Could not get faculties for university " + universityid +" - " + exc.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public List<hbmFaculty> findHbmFaculty(long universityid) {
        return null;
    }

    @Override
    public Faculty createFaculty(String name, long universityid) {
        try(Connection connection = dbService.openConnection()){
            PreparedStatement stm = connection.prepareStatement("INSERT INTO public.tfaculties(name, \"universityid\") VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, name);
            stm.setLong(2, universityid);

            int createdRows = stm.executeUpdate();
            System.out.println("Count of inserted rows - " + createdRows);

            ResultSet gentKeySet = stm.getGeneratedKeys();
            gentKeySet.next();
            int id = gentKeySet.getInt(1);

            return new Faculty(id, name, universityid);

        } catch (SQLException exc){
            System.out.println("Could not insert new university -" + exc.getMessage());
        }
        return null;
    }

    @Override
    public hbmFaculty createHbmFaculty(String name, long universityid) {
        return null;
    }

    @Override
    public List<hbmFaculty> findAllSubjectsByFaculty(Integer facId) {
        return null;
    }

    private List<Faculty> retrieveFromResultSet(ResultSet resultSet) throws  SQLException{
        List<Faculty> faculties = new ArrayList<>();
        while (resultSet.next()){
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            Long universityid = resultSet.getLong("universityid");
            Faculty faculty = new Faculty(id ,name, universityid);
            faculties.add(faculty);
        }
        return faculties;
    }
}
