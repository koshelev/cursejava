package org.lvlup.university.Repository;

import java.sql.Connection;

public interface DatabaseService {
    Connection openConnection();
    void fillPool();
}
