package org.lvlup.university.Repository;
import org.lvlup.university.domain.Faculty;
import org.lvlup.university.domain.hbmFaculty;

import java.util.List;

public interface FacultyRepository {
    List<Faculty> findFaculty(long id);
    List<hbmFaculty> findHbmFaculty(long universityid);
    Faculty createFaculty(String name, long universityid);
    hbmFaculty createHbmFaculty(String name, long universityid);
    List<hbmFaculty> findAllSubjectsByFaculty(Integer facId);
}
