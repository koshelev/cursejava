package org.lvlup.university.stream;

import java.util.*;
import java.util.stream.Collectors;

public class StreamApiExmpl {
    public static void main(String[] args){
        Collection<String> products = new ArrayList<>();
        products.add("Сало царское");
        products.add("Сало радость хохла");
        products.add("Горилка");
        products.add("Горилка Киевская");
        products.add("Сало борова");

        List<String> sortProduct = products.stream()
                .sorted()
                .collect(Collectors.toList());

        printCollection(sortProduct);

        List<String> sortProductDesc = products.stream()
                .sorted((s1, s2) -> -s1.compareTo(s2))
                .collect(Collectors.toList());

        printCollection(sortProductDesc);

        List<String> sortProductAscLen = products.stream()
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());

        printCollection(sortProductAscLen);

        List<Integer> sortIntProductAscLen = products.stream()
                .map(String::length)
                .filter(len -> len > 9)
                .collect(Collectors.toList());

        printCollection(sortIntProductAscLen);

        List<String> filtrProductAscLen =  products.stream()
                .map(String::toUpperCase)
                .filter(product -> product.startsWith("С"))
                .collect(Collectors.toList());

        printCollection(filtrProductAscLen);

    }

    private static void printCollection(Collection<?> collection){
        System.out.println("");
        collection.forEach(System.out::println);
    }

}
