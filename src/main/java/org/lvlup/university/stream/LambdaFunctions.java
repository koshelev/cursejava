package org.lvlup.university.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LambdaFunctions {

    public static void main(String[] args){

        List<Integer> values = new ArrayList<>();

        values.add(2);
        values.add(4);
        values.add(5);
        values.add(1);
        values.add(16);
        values.add(10);

//        values.sort(new Comparator<Integer>() {
//            @Override
//            public int compare(Integer integer, Integer t1) {
//                return - Integer.compare(integer, t1);
//            }
//        });

        values.sort((o1, o2) -> -Integer.compare(o1, o2));

        System.out.println(values);

        int minBound = 10;

//        values.removeIf(new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                return integer < minBound;
//            }
//        });

//        values.removeIf((val)->{
//            return val < minBound;
//        });

        values.removeIf(val -> val < minBound);
        System.out.println(values);


//      List<Integer> v =   new ArrayList<Integer>(){
//            @Override
//            public boolean add(Integer integer){
//                System.out.println("salo" + integer);
//                return super.add(integer);
//            }
//        };
//
//      v.add(2);

        for(Integer val: values){
            System.out.println(val);
        }

        values.forEach(val -> System.out.println("val" + val));

        List <String> hexVal = values.stream().map(val -> Integer.toHexString(val) + " " + Integer.toOctalString(val))
                .collect(Collectors.toList());

        hexVal.forEach(val -> System.out.println("hex/oct " + val));

        System.out.println(values.stream().findFirst());
    }
}
