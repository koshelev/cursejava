package org.lvlup.university;

import org.lvlup.university.Repository.DatabaseService;
import org.lvlup.university.Repository.FacultyRepository;
import org.lvlup.university.Repository.UniversityRepository;
import org.lvlup.university.configuration.DatabaseConfiguration;
import org.lvlup.university.domain.CountFacultiesForUniversity;
import org.lvlup.university.domain.Faculty;
import org.lvlup.university.domain.University;
import org.lvlup.university.jdbc.DatabaseServiceFactory;
import org.lvlup.university.jdbc.OriginalDatabaseService;
import org.lvlup.university.jdbc.CustomSqlException;
import org.lvlup.university.reflect.AnnotationConfigurationPropertiesProcessor;
import java.util.List;


public class UniversityApp {

    public static void main(String[] args) throws CustomSqlException {

        String confFileName = "database.properties";
        AnnotationConfigurationPropertiesProcessor.processorConfigurationFile(confFileName);
        System.out.println("Load all configuration files");
        System.out.println("University app start\n");

        DatabaseService dbService = DatabaseServiceFactory.getDatabaseService();
        dbService.fillPool();

        UniversityRepository unRep = new org.lvlup.university.Repository.jdbc.UniversityRepository(dbService);

        List<University> universities = unRep.findAllUniversity();
        for (University un : universities){
            System.out.println(un.getShortName());
        }

        System.out.println("");

        unRep.deleteUniversity(2100);

        System.out.println("");

        universities = unRep.findAllUniversity();
        for (University un : universities){
            System.out.println(un.getShortName());
        }

        System.out.println("");

        String name = "Японский автомобилестроительный";
        String shortName = "ЯАС";
        int foundationYear = 1711;
        boolean isExist= false;

        universities = unRep.findAllUniversity();
        for (University un : universities){
            if (un.getShortName().equals(shortName)) {
                isExist = true;
                break;
            };
        }

        if(!isExist) {
            University ias = unRep.createUniversity(name, shortName, foundationYear);
            System.out.println("New university - " + ias.getUniversityid() + "\n");
        } else {
            System.out.println("University is already exists \n");
        }

        universities = unRep.findAllUniversity();
        for (University un : universities){
            System.out.println(un.getUniversityid() + " | " + un.getName() + " | " + un.getShortName() + " | " + un.getFoundationYear());
            System.out.println("");
        }

        System.out.println("");
        long updId = 1228;

        University updUniv= new University((long)1228, "Сало уронили", "СУП", null);
        unRep.updateUniversity(updId, updUniv);
        unRep.updateUniversity(updId, (long)1228, "Сало уронили228!", "СУП", 228);

        universities = unRep.findAllUniversity();
        for (University un : universities){
            System.out.println(un.getUniversityid() + " | " + un.getName() + " | " + un.getShortName() + " | " + un.getFoundationYear());
            System.out.println("");
        }

        System.out.println("Find one university");

        universities = unRep.findUniversity(1228);
        for (University un : universities){
            System.out.println(un.getUniversityid() + " | " + un.getName() + " | " + un.getShortName() + " | " + un.getFoundationYear());
            System.out.println("");
        }

        System.out.println("");

        FacultyRepository facRep = new org.lvlup.university.Repository.jdbc.FacultyRepository(dbService);

        long universityidForNewFaculty = 1228;
        boolean isUniversityNotExist= true;

        universities = unRep.findAllUniversity();
        for (University un : universities){
            if (un.getUniversityid() == universityidForNewFaculty) {
                isUniversityNotExist = false;
                break;
            };
        }

        if(!isUniversityNotExist) {
            Faculty fac = facRep.createFaculty("fairLady z", universityidForNewFaculty);
            System.out.println("New faculty was added, with id - " + fac.getId() + "\n");
        } else {
            throw new CustomSqlException("Faculty wasn't added, University is not exists\n");
        }

        System.out.println("Faculties list for university id - " + universityidForNewFaculty );
        List<Faculty> faculties = facRep.findFaculty(universityidForNewFaculty);
        for (Faculty fc : faculties){
            System.out.println(fc.getId() + " | " + fc.getName() + " | " + fc.getUniversityid());
            System.out.println("");
        }

        System.out.println("");

        System.out.println("Faculties cnt for universities");
        List<CountFacultiesForUniversity> countFacultiesForUniversities = unRep.countFacultiesForUniversities();
        for (CountFacultiesForUniversity cff : countFacultiesForUniversities){
            System.out.println(cff.getuniversityid() + " | " + cff.getFacultiesCnt());
            System.out.println("");
        }
        System.out.println("");

    }
}
