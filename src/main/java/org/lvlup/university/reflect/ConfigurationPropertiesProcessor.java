package org.lvlup.university.reflect;

import org.lvlup.university.configuration.DatabaseConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationPropertiesProcessor {
    public static void processorConfigurationFile(String fileName) {

        InputStream inp = ConfigurationPropertiesProcessor.class.getClassLoader().getResourceAsStream(fileName);
        try (BufferedReader fileReader = new BufferedReader((new InputStreamReader(inp)));) {
            Map<String, Object> configurationProperties = readProperties(fileReader);
            fillConfiguration(configurationProperties);
        } catch (IOException exc) {
            System.out.println("Err load prop" + fileName);
            throw new RuntimeException(exc);
        } catch (IllegalAccessException exc){
            System.out.println("Could not set prop value to object field");
            throw new RuntimeException(exc);
        }
    }

    private static Map<String, Object> readProperties(BufferedReader reader) throws IOException {
        Map<String, Object> properties = new HashMap<>();
        String line;

        while ((line = reader.readLine()) != null) {
            if (!line.isBlank()) {
                String[] elements = line.split("=");
                properties.put(
                        elements[0].trim().replace("database","").replace(".",""),
                        elements[1].trim());
            }
        }
    return properties;
    }

    private static void fillConfiguration(Map<String, Object> properties) throws IllegalAccessException{
        Class<?> dbConfigurationClass = DatabaseConfiguration.class;
        Field[] fields  = dbConfigurationClass.getDeclaredFields();
        for(Field field: fields){
         if(!field.getName().equalsIgnoreCase("instance")) {
             String lowerCaseFieldName = field.getName().toLowerCase();
             Object propertyValue = properties.get(lowerCaseFieldName);
             field.setAccessible(true);
             field.set(DatabaseConfiguration.getInstance(), castStringToFieldType(field.getType(), propertyValue));
         }
        }
    }

    private static Object castStringToFieldType(Class<?> fieldType, Object propertyValue){
        if(fieldType == String.class){
            return propertyValue;
        }

        if(fieldType.isPrimitive() && fieldType != boolean.class){
            return Integer.parseInt((String) propertyValue);
        }
        return null;
    }
}