package org.lvlup.university.reflect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RandomInt {
    int maxArgs() default Integer.MAX_VALUE;
    int minArgs() default Integer.MIN_VALUE;
}
