package org.lvlup.university.configuration;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.lvlup.university.domain.*;
import org.postgresql.Driver;

import java.util.HashMap;
import java.util.Map;

public class HibernateConfiguration {

    private static SessionFactory factory;

    public static void configure(DatabaseConfiguration dbConfiguration) {
        Map<String,String> hibernateProperties = buildHibernateProperties(dbConfiguration);
        StandardServiceRegistry ssr =  new StandardServiceRegistryBuilder()
                .applySettings(hibernateProperties)
                .build();

        Configuration cfg = new Configuration().addAnnotatedClass(University.class)
                .addAnnotatedClass(hbmFaculty.class)
                .addAnnotatedClass(Subjects.class)
                .addAnnotatedClass(FacultyInfo.class)
                .addAnnotatedClass(FacultySubject.class);
        factory = cfg.buildSessionFactory(ssr);
    }

    private static Map<String, String> buildHibernateProperties(DatabaseConfiguration dbConfiguration){
        Map<String,String> properties = new HashMap<>();

        properties.put("hibernate.connection.driver_class", Driver.class.getName());

        properties.put("hibernate.connection.url", dbConfiguration.getUrl());
        properties.put("hibernate.connection.username", dbConfiguration.getLogin());
        properties.put("hibernate.connection.password", dbConfiguration.getPassword());

        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");

        properties.put("hibernate.hbm2ddl.auto", "validate"); //Отвечает за генерацию схемы, validate, update, create, create-drop

        return properties;

    }

    public static SessionFactory getSessionFactory() {
        if (factory == null) {
            throw new RuntimeException("Session factory is null");
        }
        return factory;
    }
}
