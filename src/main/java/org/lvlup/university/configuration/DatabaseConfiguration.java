package org.lvlup.university.configuration;

import org.lvlup.university.reflect.Property;

public class DatabaseConfiguration {

    @Property(key ="database.url")
    private String url;
    @Property(key ="database.login")
    private String login;
    @Property(key ="database.password")
    private String password;

    @Property(key ="database.min.pool.size")
    private int minPoolSize;

    @Property(key ="database.connection.timeout")
    private long connectionTimeout;

    @Property(key ="database.read.timeout")
    private  long readTimeout;

    @Override
    public String toString() {
        return "DatabaseConf{" +
                "url='" + url + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", minPoolSize=" + minPoolSize +
                ", connectionTimeout=" + connectionTimeout +
                '}';
    }

    private DatabaseConfiguration(){}

    private static final DatabaseConfiguration INSTANCE = new DatabaseConfiguration();

    public static DatabaseConfiguration getInstance(){
        return INSTANCE;
    }

    public String getUrl() {
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public long getConnectionTimeout() {
        return connectionTimeout;
    }

}
