package org.lvlup.university.configuration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.lvlup.university.Repository.hbm.HibernateFacultyRepository;
import org.lvlup.university.Repository.hbm.HibernateUniversityRepository;
import org.lvlup.university.domain.University;
import org.lvlup.university.domain.hbmFaculty;

public class HibernateFacultyRepositoryIT {
    private static  HibernateUniversityRepository universityRepository;
    private static HibernateFacultyRepository facultyRepository;

    @BeforeAll
    public static void initializeRepository(){
        universityRepository = new HibernateUniversityRepository(TestHibernateConfiguration.getFactory());
        facultyRepository = new HibernateFacultyRepository(TestHibernateConfiguration.getFactory());
    }

    @Test
    public void shouldCreateFaculty(){
        University university = universityRepository.createUniversity("Salo", "s", 1229);
        String name = "Civic";

        hbmFaculty faculty = facultyRepository.createHbmFaculty(name, university.getUniversityid());

        Assertions.assertNotNull(faculty.getId());
        Assertions.assertEquals(name, faculty.getName());
        //Assertions.assertEquals(university, faculty.getUniversity());
    }
}
