package org.lvlup.university.configuration;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.lvlup.university.domain.*;
import org.postgresql.Driver;

import java.util.HashMap;
import java.util.Map;

public class TestHibernateConfiguration {
    private static SessionFactory factory;

    public static SessionFactory getFactory(){
        return factory;
    }

    static {

        Map<String,String> properties = new HashMap<>();

        properties.put("hibernate.connection.driver_class", Driver.class.getName());

        properties.put("hibernate.connection.url","jdbc:postgresql://localhost:5432/university-it" );
        properties.put("hibernate.connection.username","rector");
        properties.put("hibernate.connection.password", "rector228");

        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "create"); //Отвечает за генерацию схемы, validate, update, create, create-drop


        StandardServiceRegistry ssr =  new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        Configuration cfg = new Configuration()
                .addAnnotatedClass(University.class)
                .addAnnotatedClass(hbmFaculty.class)
                .addAnnotatedClass(Subjects.class)
                .addAnnotatedClass(FacultyInfo.class)
                .addAnnotatedClass(FacultySubject.class);
        factory = cfg.buildSessionFactory(ssr);

    }
}
