package org.lvlup.university.jdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.postgresql.jdbc.PgConnection;
import org.postgresql.util.HostSpec;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class ConnectionPoolTest {

    @Test
    public void shouldReturnNullIfQueueIsEmpty(){
        ConnectionPool pool = new ConnectionPool();
        Connection result = pool.getConnection();
        Assertions.assertNull(result);
    }

    @Test
    public void shouldOfferConnectionToPool() {
        ConnectionPool pool = new ConnectionPool();
        Connection connection = Mockito.mock(Connection.class);

        pool.returnConnection(connection);

        Connection result = pool.getConnection();
        Assertions.assertSame(connection, result);
    }

}