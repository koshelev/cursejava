package org.lvlup.university.hbm;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.lvlup.university.Repository.hbm.HibernateFacultyRepository;
import org.lvlup.university.domain.University;
import org.lvlup.university.domain.hbmFaculty;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;



public class HibernateFacultyRepositoryTest {
    private SessionFactory factory;
    private Session session;
    private Transaction transaction;

    private HibernateFacultyRepository hibernateFacultyRepository;

    @BeforeEach
    public void initializeMocks(){
        factory = Mockito.mock(SessionFactory.class);
        session = Mockito.mock(Session.class);
        transaction = Mockito.mock(Transaction.class);

        Mockito.when(factory.openSession()).thenReturn(session);
        Mockito.when(session.beginTransaction()).thenReturn(transaction);

        hibernateFacultyRepository = new HibernateFacultyRepository(factory);
    }

    @Test
    public void shouldCreateFaculty() {

      String facultyName = "300zx";
      long universityId = 228L;

        University university = new University();
        university.setUniversityid(universityId);
        //Mockito.when(session.load(University.class, universityId)).thenReturn(university);
        Mockito.when(session.load(ArgumentMatchers.eq(University.class), ArgumentMatchers.anyLong())).thenReturn(university);

        hbmFaculty result =  hibernateFacultyRepository.createHbmFaculty(facultyName, universityId);

        Assertions.assertEquals(facultyName, result.getName());
        Assertions.assertEquals(universityId, result.getUniversity().getUniversityid());


        ArgumentCaptor<hbmFaculty> facultyArgumentCaptor = ArgumentCaptor.forClass(hbmFaculty.class);
        Mockito.verify(session).persist(facultyArgumentCaptor.capture());

        hbmFaculty captFaculty = facultyArgumentCaptor.getValue();

        Assertions.assertEquals(facultyName, captFaculty.getName());

        //Mockito.verify(session).persist(ArgumentMatchers.any());
        //Mockito.verify(session).persist(result);
        Mockito.verify(transaction, Mockito.times(1)).commit();
        Mockito.verify(session).close();

    }

    @Test
    public void shouldCloseSessionInCreateFacultyEvenExceptionWasThrown() {
        Mockito.when(session
                .load(University.class, 228L))
                .thenThrow(HibernateException.class);

        Mockito.doThrow(HibernateException.class).when(session).persist(ArgumentMatchers.any(hbmFaculty.class));

        Assertions.assertThrows(HibernateException.class,
                () -> hibernateFacultyRepository.createHbmFaculty("salo", 228L)
                );

        Mockito.verify(session).close();
    }
}
